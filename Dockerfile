FROM openjdk:17
EXPOSE 8080
COPY target/docker-first.jar /docker-first.jar
ENTRYPOINT ["java","-jar","/docker-first.jar"]
