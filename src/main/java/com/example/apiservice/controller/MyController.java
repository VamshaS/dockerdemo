package com.example.apiservice.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController

@RequestMapping("/sample")

public class MyController {
	@GetMapping("/hi")
	public String demo() {
		return "Hello World";
	}
}
